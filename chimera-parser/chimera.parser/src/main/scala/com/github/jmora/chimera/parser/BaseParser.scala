package com.github.jmora.chimera.parser

import com.github.jmora.chimera.parser.ParserClasses._
import scala.util.parsing.combinator.syntactical._
import scala.util.parsing.combinator.ImplicitConversions
import scala.util.parsing.combinator.PackratParsers
import scala.util.parsing.combinator.RegexParsers

class BaseParser extends RegexParsers with ImplicitConversions {
    val eol = sys.props("line.separator")
    override val whiteSpace = """[ \t]+""".r

    def rules = repsep(rule, eol) ^^ { Rules(_) }
    def rule = id ~ "=" ~ repsep(sequence, "|") ^^ flatten3 { (e1: ID, _: Any, e3: List[Sequence]) => Rule(e1, e3) }
    def sequence: Parser[Sequence] = rep(optional) ~ expression ~ rep(opexpression) ^^ { (x: List[Expression], y: Expression, z: List[Expression]) => Sequence(x ++ (y :: z)) } // check what is more efficient, prepend or append
    def opexpression: Parser[Expression] = (repetition | element | parenthesized | optional)
    def expression: Parser[Expression] = (repetition | element | parenthesized)
    def parenthesized: Parser[Sequence] = "(" ~> sequence <~ ")" ^^ { x => x }
    def repetition: Parser[Expression] = (element <~ "*" | parenthesized <~ "*") ^^ { Repetition(_) }
    def optional: Parser[Expression] = "[" ~> sequence <~ "]" ^^ { Optional(_) }
    def element: Parser[Element] = (id | constant) ^^ { x => x }
    def constant: Parser[Constant] = """\"(\\.||[^"])*\"""".r ^^ { Constant(_) }
    def id: Parser[ID] = """\w+""".r ^^ { ID(_) }

    def parse(text: String): Option[Rules] = {
        //val s = rules(new lexical.Scanner(text))
        val s = parseAll(rules, text)
        s match {
            case Success(res, next) => {
                println("Victoly!\n" + res.toString)
                Some(res)
            }
            case Error(msg, next) => {
                println("error: " + msg)
                None
            }
            case Failure(msg, next) => {
                println("failure: " + msg)
                None
            }
        }
        /* 
         * Important: Remove ambiguous recursion (you did this with repetition, it's not of an expression, but only two possible types of expressions,
         * according to the rules, either an element or a parenthesized, this removes ambiguousness and lowers the iteration to avoid loops
         * split expression and opexpression
         * no recursion on expression, but through parenthesized (to add parentheses, which does also provide a condition for terminatio)
         * beware of things failing after consuming the input successfully 
         * */

    }
}