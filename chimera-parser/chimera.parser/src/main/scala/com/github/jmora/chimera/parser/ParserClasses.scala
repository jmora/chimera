package com.github.jmora.chimera.parser

object ParserClasses {
    abstract class Element extends Expression

    case class ID(value: String) extends Element {
        override def toString(): String = value
    }

    case class Constant(value: String) extends Element {
        override def toString(): String = value
    }

    abstract class Expression

    case class Repetition(value: Expression) extends Expression {
        override def toString(): String = s"($value)*"
    }

    case class Optional(value: Expression) extends Expression {
        override def toString() = s"[$value]"
    }

    case class Sequence(value: List[Expression]) extends Expression {
        override def toString() = s"(${value.mkString(" ")})"
    }

    /* case class Sequence(head: Element, tail: Expression) extends Expression {
        override def toString() = s"$head $tail"
    } */

    case class Rule(head: ID, body: List[Expression]) {
        override def toString() = s"$head = ${body.mkString(" | ")}"
    }

    case class Rules(rules: List[Rule]) {
        override def toString() = rules.mkString("\n")
    }
}