package com.github.jmora.chimera.parser

/**
 * @author ${user.name}
 */
object App {

    def main(args: Array[String]) {
        val parser: BaseParser = new BaseParser
        val eol = sys.props("line.separator")
        parser.parse(s"m=n${eol}a = c (d e)* f | [g*] [(hi [j k])* l] m [[n] o p ] ")
    }

    //parser.parse("select = select columns from tables\n" + "columns = columnhead columntail\n" + "columntail = (, column)*" + "column = id\n" + "table = id\n")
    /* parser.parse("a = b \n a = c \n ")  match {
            case Some(r) => {
                println(r.toString())
            }
            case None => println("darn...")
        } */

}
